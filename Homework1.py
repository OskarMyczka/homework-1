#!/usr/bin/env python
""" Oskar Myczka - homework 1 """

height = float(1.82)

print("I'm:", height, "m tall")

name = str("Oskar Myczka")

print("My name is: " + name)

age = int(21)

print("I'm:", age)
